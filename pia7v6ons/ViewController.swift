//
//  ViewController.swift
//  pia7v6ons
//
//  Created by Bill Martensson on 2017-10-04.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    
    
    @IBOutlet weak var addTextfield: UITextField!
    
    @IBOutlet weak var todoTableview: UITableView!
    
    
    @IBOutlet weak var tableviewBottomDistance: NSLayoutConstraint!
    
    
    var todo = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(notification:)), name: .UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: .UIKeyboardDidHide, object: nil)
        
        let defaults = UserDefaults.standard
        
        if let savedList = defaults.array(forKey: "todolist") as? [String]
        {
            todo = savedList
        }
        
        //todoTableview.isEditing = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func keyBoardWillShow(notification: NSNotification) {
        //handle appearing of keyboard here
        print("KEYBAORD SHOW")
        
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
        
            tableviewBottomDistance.constant = -keyboardHeight
            view.layoutIfNeeded()
        }
        
        
    }
    
    
    @objc func keyBoardWillHide(notification: NSNotification) {
        //handle dismiss of keyboard here
        print("KEYBOARD HIDE")
        
        
        tableviewBottomDistance.constant = 0
        view.layoutIfNeeded()
    }
    
    @IBAction func addToList(_ sender: Any) {
        addNewThingToList()
    
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        addNewThingToList()
        
        
        return true
    }
    
    func addNewThingToList()
    {
        if(addTextfield.text == "")
        {
            return
        }
        
        todo.append(addTextfield.text!)
        
        let defaults = UserDefaults.standard
        
        defaults.set(todo, forKey: "todolist")
        defaults.synchronize()
        
        todoTableview.reloadData()
        
        let theRowToScrollTo = IndexPath(row: todo.count-1, section: 0)
        
        todoTableview.scrollToRow(at: theRowToScrollTo, at: .bottom, animated: true)
        
        addTextfield.text = ""
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "todo")!
        
        cell.textLabel?.text = todo[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        todo.remove(at: indexPath.row)
        
        let defaults = UserDefaults.standard
        
        defaults.set(todo, forKey: "todolist")
        defaults.synchronize()
        
        todoTableview.reloadData()
    }

    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        if(indexPath.row == 0)
        {
            return nil
        }
        
        
        let modifyAction = UIContextualAction(style: .normal, title:  "Update", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            print("Update action ...")
            success(true)
        })
        modifyAction.backgroundColor = .blue
        
        let deleteAction = UIContextualAction(style: .normal, title:  "Delete", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            print("Delete action ...")
            
            self.todo.remove(at: indexPath.row)
            
            let defaults = UserDefaults.standard
            
            defaults.set(self.todo, forKey: "todolist")
            defaults.synchronize()
            
            self.todoTableview.reloadData()
            
            success(true)
        })
        deleteAction.backgroundColor = .red
        
        
        return UISwipeActionsConfiguration(actions: [modifyAction, deleteAction])
        
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        
        let modifyAction = UIContextualAction(style: .normal, title:  "Do stuff", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            print("Update action ...")
            success(true)
        })
        modifyAction.backgroundColor = .blue
    
        
        
        return UISwipeActionsConfiguration(actions: [modifyAction])
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        let theMovedString = todo[sourceIndexPath.row]
        todo.remove(at: sourceIndexPath.row)
        
        todo.insert(theMovedString, at: destinationIndexPath.row)
        
        todoTableview.reloadData()
    }
    
}

